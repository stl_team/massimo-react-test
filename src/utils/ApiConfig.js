import axios from 'axios';
import history from '../utils/history';

const axiosInstance = () => {
    const defaultOptions = {
        baseURL: 'http://161.97.170.81:3010/api/',
    };

    // Create instance
    let instance = axios.create(defaultOptions);

    instance.interceptors.response.use(
        function (response) {
            return response;
        },
        function (err) {
            if (
                err &&
                err.response &&
                err.response.status &&
                err.response.status === 401 &&
                err.config &&
                !err.config.__isRetryRequest
            ) {
                history.push('/logout');
            }
            return Promise.reject(err);
        }
    );

    return instance;
};

export default axiosInstance();
