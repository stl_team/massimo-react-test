import history from './utils/history';
import { Switch, Route, Redirect, Router } from 'react-router-dom';
import { ToastProvider } from 'react-toast-notifications';
import './App.css';
import Home from './containers/Home/home';
import Page from './containers/Page/page';

function App() {
    return (
        <ToastProvider>
            <Router history={history}>
                <Switch>
                    <Route
                        exact
                        path='/'
                        component={() => <Redirect to='/home' />}
                    />
                    <Route exact path='/home' component={Home} />
                    <Route exact path='/page/:id' component={Page} />
                </Switch>
            </Router>
        </ToastProvider>
    );
}

export default App;
