import React, { useEffect, useCallback, useState } from 'react';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useToasts } from 'react-toast-notifications';
import { GetSecurityData } from '../../server/action';
import history from '../../utils/history';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
        width: '100%',
        position: 'absolute',
        top: 50,
    },
    buttonCls: {
        margin: theme.spacing(1),
    },
    addBtn: {
        marginTop: 17,
    },
}));

export default function Page() {
    const classes = useStyles();
    const [state, setState] = useState([]);
    const [callback, setCallback] = useState([]);
    const { addToast } = useToasts();
    const { id } = useParams();
    const successCB = useCallback(
        (res, statusFlag) => {
            if (statusFlag === 1) {
                setState(res?.data?.data);
            } else {
                addToast(res, {
                    appearance: 'error',
                    autoDismiss: true,
                });
            }
        },
        [addToast]
    );

    useEffect(() => {
        let page = [1, 2];
        if (page.indexOf(parseInt(id)) > -1) {
            GetSecurityData(successCB, parseInt(id));
        } else {
            addToast('Page not found.', {
                appearance: 'error',
                autoDismiss: true,
            });
            history.push('/home');
        }
    }, [addToast, id, successCB]);

    const columns = [
        { title: 'Column 1', field: 'col1' },
        {
            title: 'Column 2',
            field: 'col2',
            render: (rowdata) => (
                <>
                    <TextField
                        id='outlined-basic'
                        label='Type Something'
                        variant='outlined'
                        helperText={callback[rowdata?.tableData?.id]}
                        onChange={(e) => {
                            setCallback({
                                ...callback,
                                [rowdata?.tableData?.id]: e?.target?.value,
                            });
                        }}
                    />
                </>
            ),
        },
        {
            title: 'Action',
            field: 'action',
            render: () => (
                <>
                    {state?.access && state?.access.indexOf('EDIT') > -1 && (
                        <Button
                            variant='contained'
                            color='primary'
                            className={classes.buttonCls}
                        >
                            Edit
                        </Button>
                    )}
                    {state?.access && state?.access.indexOf('DELETE') > -1 && (
                        <Button
                            variant='contained'
                            color='secondary'
                            className={classes.buttonCls}
                        >
                            Delete
                        </Button>
                    )}
                    {state?.access && state?.access.indexOf('VIEW') > -1 && (
                        <Button
                            variant='contained'
                            color='default'
                            className={classes.buttonCls}
                        >
                            View
                        </Button>
                    )}
                </>
            ),
        },
    ];

    const dummyList = [
        {
            col1: 'Fake Data 1',
            col2: 'textInput',
            action: '',
        },
        {
            col1: 'Fake Data 2',
            col2: 'textInput',
            action: '',
        },
    ];

    return (
        <div className={classes.root}>
            {state?.view && state?.view.indexOf('ADD') > -1 && (
                <Button
                    variant='contained'
                    color='primary'
                    className={classes.addBtn}
                >
                    Add
                </Button>
            )}
            {state?.view && state?.view.indexOf('SEARCH') > -1 && (
                <TextField
                    id='outlined-basic'
                    label='Search'
                    variant='outlined'
                />
            )}
            {state?.view && state?.view.indexOf('GRID') > -1 && (
                <MaterialTable
                    title='Grid'
                    columns={columns}
                    data={dummyList}
                    options={{
                        search: false,
                        paging: false,
                    }}
                />
            )}
        </div>
    );
}
