import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import history from '../../utils/history';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        position: 'absolute',
        top: '50%',
        display: 'flex',
        justifyContent: 'center',
    },
    buttonCls: {
        margin: theme.spacing(1),
    },
}));

export default function Home() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Button
                variant='contained'
                color='primary'
                onClick={() => history.push('/page/1')}
                className={classes.buttonCls}
            >
                full access
            </Button>
            <Button
                variant='contained'
                color='secondary'
                onClick={() => history.push('/page/2')}
                className={classes.buttonCls}
            >
                restricted access
            </Button>
        </div>
    );
}
