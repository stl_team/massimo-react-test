import axios from '../utils/ApiConfig';

const SecurityAPI = 'v1/security/';

export const GetSecurityData = async (successCB, params) => {
    try {
        let res = await axios.get(SecurityAPI + params);
        successCB(res, 1);
    } catch (e) {
        successCB(e?.response?.data?.message, 0);
    }
};
